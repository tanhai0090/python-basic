### Writen Function Hypotenuse ###

#PART 1
import math
def Hypotennuse(a, b):
    return math.sqrt(a**2 + b**2)
### OUTPUT ###
print("length of the hypotenuse of a right triangle",Hypotennuse(3,4))
#5
print("length of the hypotenuse of a right triangle",Hypotennuse(10,20))
#22.360679774997898
print("length of the hypotenuse of a right triangle",Hypotennuse(14,23))
#26.92582403567252



#PART 2
def rhombus_area(d1,d2):
    return 1/2*(d1*d2)

print(rhombus_area(2,5))
#5.0
print(rhombus_area(10,5))
#25.0
print(rhombus_area(25,5))
#62.5



#PART 2.1
def divide(x, y): 
    try:
        result = x // y 
        print("Yeah ! Your answer is :", result)
    except ZeroDivisionError: 
        print("Sorry ! You are dividing by zero ")

divide(3,5)
#Yeah ! Your answer is : 0
divide(10,0)
#Sorry ! You are dividing by zero 
divide(5,5)
#Yeah ! Your answer is : 1
    



