import math
def my_sqrt(a):
    x = a / 2
    while True:
        y = (x + a/x) / 2.0
        if y == x:
            break
        x = y
    return x

def test_sqrt():
    a = 0
    while (a < 25):
        a = a + 1
        my_sqrt(a)
        math.sqrt(a)
        diff = abs(math.sqrt(a) - my_sqrt(a))
        print("a =", a, "| my_sqrt(a) =", my_sqrt(a), "| math.sqrt =", math.sqrt(a), "| diff =", diff)
    return

test_sqrt()